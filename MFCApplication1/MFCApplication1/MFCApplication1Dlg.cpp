
// MFCApplication1Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "MFCApplication1.h"
#include "MFCApplication1Dlg.h"
#include "afxdialogex.h"
#include "fltuser.h"
#include <atlstr.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"fltlib.lib")
#pragma comment(lib,"fltmgr.lib")



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCApplication1Dlg dialog



CMFCApplication1Dlg::CMFCApplication1Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFCAPPLICATION1_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCApplication1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMFCApplication1Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CMFCApplication1Dlg::OnBnClickedButton1)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CMFCApplication1Dlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON2, &CMFCApplication1Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CMFCApplication1Dlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// CMFCApplication1Dlg message handlers



BOOL CMFCApplication1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	a = (CEdit*)GetDlgItem(IDC_EDIT2);
	list = (CComboBox*)GetDlgItem(IDC_COMBO1);
	getlist = (CListBox*)GetDlgItem(IDC_LIST1);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMFCApplication1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMFCApplication1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMFCApplication1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
HANDLE port = NULL;
DWORD byterce = 0;
char recbuffer[200] = { 0 };
CString sData;
CString sData2;

void CMFCApplication1Dlg::send_Message(char* pchar, int len)
{
	if (port == NULL)
	{
		if (FilterConnectCommunicationPort(L"\\mf", 0, NULL, 0, NULL, &port))
		{
			MessageBoxA(0, "Driver is not installed.", "Error", 0);
			return;
		}
	}

	if (!(FilterSendMessage(port, pchar, len, recbuffer, 200, &byterce)))
	{
		//strcpy(recbuffer, "#");
		getlist->ResetContent();

		CString First;
		CString Last;

		First = "";
		Last = "";

		bool isFirst = true;

		for (int i = 0; i < 200; i++)
		{
			if (recbuffer[i] == 0 && First != "" && Last != "")
			{
				getlist->AddString(First + " " + Last);
				return;
			}

			if (recbuffer[i] == 0 && First[0] == '#')
				return;

			if (recbuffer[i] == '#')
			{
				if (First != "" && Last != "")
				{
					getlist->AddString(First + " " + Last);
					Last = "";
					First = "";
					isFirst = true;
				}
				else if (First != "" && Last == "")
				{
					isFirst = !isFirst;
				}
				continue;
			}

			if (isFirst)
			{
				First += recbuffer[i];
			}
			else
			{
				Last += recbuffer[i];
			}
		}
	}
}

void CMFCApplication1Dlg::OnBnClickedButton1()
{
	int index = getlist->GetAnchorIndex();
	if (index != -1)
	{
		CString item;
		getlist->GetText(index, item);
		getlist->DeleteString(index);
		int len = item.GetLength();
		char* pchar = new char[len + 2];
		pchar[0] = '#';
		for (int i = 0; i < len; i++)
		{
			if (item[i] == ' ')
				pchar[i + 1] = '#';
			else
				pchar[i + 1] = item[i];
		}
		pchar[len + 1] = 0;

		send_Message(pchar, len + 1);
	}
	else
	{
		MessageBoxA(0, "Permission not selected.", "Error", 0);
	}
}

void CMFCApplication1Dlg::OnBnClickedButton2()
{
	//getlist->AddString(_T("mspaint jpg"));
	sData = "";
	sData2 = "";

	int nSel = list->GetCurSel();
	list->GetLBText(nSel, sData);
	a->GetWindowTextW(sData2);

	if (sData2.GetLength() > 30) {
		MessageBoxA(0, "The length is too long.", "Error", 0);
		return;
	}
	for (int i = 0; i < sData2.GetLength(); i++) {
		if (sData2[i] == '#') {
			MessageBoxA(0, "Invalid characters.", "Error", 0);
			return;
		}
	}

	sData2 += " ";
	for (int i = 1; i < sData.GetLength(); i++)
	{
		sData2 += sData.GetAt(i);
	}


	if (getlist->FindString(0, sData2) != -1)
	{
		MessageBoxA(0, "The permission exists.", "Error", 0);
	}
	else
	{
		list->GetLBText(nSel, sData);
		a->GetWindowTextW(sData2);

		sData2 += "#";
		for (int i = 1; i < sData.GetLength(); i++)
		{
			sData2 += sData.GetAt(i);
		}

		int len = sData2.GetLength();
		char* pchar = new char[len];
		for (int i = 0; i < len; i++)
		{
			pchar[i] = sData2[i];
		}
		send_Message(pchar, len);
	}
}
void CMFCApplication1Dlg::OnCbnSelchangeCombo1()
{

}

void CMFCApplication1Dlg::OnBnClickedButton3()
{
	sData2 = "##";

	int len = sData2.GetLength();
	char* pchar = new char[len];
	for (int i = 0; i < len; i++)
	{
		pchar[i] = sData2[i];
	}
	send_Message(pchar, len);
}
