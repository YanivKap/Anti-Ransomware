#pragma once
#include <fltKernel.h>
#include <dontuse.h>
#include <suppress.h>

#define NumOfPermissions 2
#define NumOfGlobalPermissions 14
#define NameLen 200
#define UserInputsLen 30
#define ReturnedLen 199

typedef struct {
	WCHAR Extension[NameLen];
	int ProgramNamesCounter;
	WCHAR* ProgramNames[];
} Permission;

typedef struct UserPermission {
	WCHAR userProgramName[UserInputsLen];
	WCHAR userExtension[UserInputsLen];
	struct UserPermission* next;
} UserPermission, *PUserPermission;

typedef NTSTATUS(*QUERY_INFO_PROCESS) (
	__in HANDLE ProcessHandle,
	__in PROCESSINFOCLASS ProcessInformationClass,
	__out_bcount(ProcessInformationLength) PVOID ProcessInformation,
	__in ULONG ProcessInformationLength,
	__out_opt PULONG ReturnLength
	);
QUERY_INFO_PROCESS ZwQueryInformationProcess;

extern PUserPermission userPermissionHead;
extern KSPIN_LOCK ListLock;
extern CHAR OutputBufferList[ReturnedLen];

VOID CreateReturnedData();

//Remove a Permission node to list of data that we recieve from the user.
NTSTATUS RemoveUserPermissionList(PCHAR ProgramAndExtension, ULONG InputBufferLength, PKLOCK_QUEUE_HANDLE PListLockHandle);

//Add a Permission node to list of data that we recieve from the user.
NTSTATUS AddUserPermissionList(PUserPermission userPermissionNode, PKLOCK_QUEUE_HANDLE PListLockHandle);

//Check if the program is allowed to touch the file by the users permissions.
NTSTATUS IsPermissionUser(int isNewExtension);

//Check if the program process is a system process and needs to be allowed.
NTSTATUS IsProgramAllowedGlobal();

//Free the Permission list the we recieve from the user.
VOID FreeUserPermissionList();

//Parses the user mode msg to extensions and programs and creates a permission struct.
NTSTATUS ParseUserMsg(PCHAR InputBuffer, ULONG InputBufferLength);

//kills the process that requested the driver.
NTSTATUS killProcess(_Inout_ PFLT_CALLBACK_DATA Data);

//finds program's name.
NTSTATUS GetProcessImageName();

//runs the permissions check.
NTSTATUS PermissionCheck();

//The main function that is called in every preoperation. The logic of the driver.
NTSTATUS Handler(_Inout_ PFLT_CALLBACK_DATA Data);

