#include "Helper.h"

WCHAR gProgramName[NameLen] = { 0 }, gExtension[NameLen] = { 0 };
CHAR OutputBufferList[ReturnedLen] = { 0 };

VOID CreateReturnedData()
{
	size_t i = 0, j = 0;
	PUserPermission userPermissionNode = userPermissionHead;
	RtlFillMemory(OutputBufferList, ReturnedLen, 0);

	OutputBufferList[i] = '#';
	i++;

	while (userPermissionNode != NULL)
	{
		j = 0;
		while (i < ReturnedLen && userPermissionNode->userProgramName[j] != 0)
		{
			OutputBufferList[i] = userPermissionNode->userProgramName[j];
			i++;
			j++;
		}

		OutputBufferList[i] = '#';
		i++;

		j = 0;
		while (i < ReturnedLen && userPermissionNode->userExtension[j] != 0)
		{
			OutputBufferList[i] = userPermissionNode->userExtension[j];
			i++;
			j++;
		}

		OutputBufferList[i] = '#';
		i++;

		userPermissionNode = userPermissionNode->next;
	}
}

NTSTATUS RemoveUserPermissionList(PCHAR ProgramAndExtension, ULONG InputBufferLength, PKLOCK_QUEUE_HANDLE PListLockHandle)
{
	PUserPermission userPermissionNode = NULL, prev = NULL;
	WCHAR WProgramAndExtension[UserInputsLen] = { 0 };

	if (InputBufferLength <= UserInputsLen)
	{
		for (size_t i = 0; i < InputBufferLength; i++)
		{
			WProgramAndExtension[i] = (WCHAR)ProgramAndExtension[i];
		}

		_wcsupr(WProgramAndExtension);
	}

	//touching the shared resource->
	KeAcquireInStackQueuedSpinLock(&ListLock, PListLockHandle);

	userPermissionNode = userPermissionHead;

	// If head node itself holds the key to be deleted
	if (userPermissionNode != NULL && (wcsstr(WProgramAndExtension, userPermissionNode->userProgramName) != NULL && wcsstr(WProgramAndExtension, userPermissionNode->userExtension) != NULL))
	{
		userPermissionHead = userPermissionNode->next;// Changed head
		ExFreePoolWithTag((void *)userPermissionNode, '9gaT');// free old head
		return STATUS_SUCCESS;
	}

	// Search for the key to be deleted, keep track of the
	// previous node as we need to change 'prev->next'
	while (userPermissionNode != NULL && !((wcsstr(WProgramAndExtension, userPermissionNode->userProgramName) != NULL && wcsstr(WProgramAndExtension, userPermissionNode->userExtension) != NULL)))
	{
		prev = userPermissionNode;
		userPermissionNode = userPermissionNode->next;
	}

	// If key was not present in linked list
	if (userPermissionNode == NULL)
		return STATUS_UNSUCCESSFUL;

	// Unlink the node from linked list
	prev->next = userPermissionNode->next;
	ExFreePoolWithTag((void *)userPermissionNode, '9gaT');// Free memory

	return STATUS_SUCCESS;
}

NTSTATUS AddUserPermissionList(PUserPermission userPermissionNode, PKLOCK_QUEUE_HANDLE PListLockHandle)
{
	//touching the shared resource->
	KeAcquireInStackQueuedSpinLock(&ListLock, PListLockHandle);
	userPermissionNode->next = userPermissionHead;
	userPermissionHead = userPermissionNode;

	return STATUS_SUCCESS;
}

VOID FreeUserPermissionList()
{
	PUserPermission userPermissionNode = NULL;

	while ((userPermissionNode = userPermissionHead) != NULL)
	{
		userPermissionHead = userPermissionHead->next;
		ExFreePoolWithTag((void *)userPermissionNode, '9gaT');
	}
}

NTSTATUS ParseUserMsg(PCHAR InputBuffer, ULONG InputBufferLength)
{
	int flag = 0, len = 0;
	NTSTATUS status;
	KLOCK_QUEUE_HANDLE ListLockHandle;

	if (InputBufferLength == 2 && InputBuffer[0] == '#' && InputBuffer[1] == '#')
	{
		//touching the shared resource->
		KeAcquireInStackQueuedSpinLock(&ListLock, &ListLockHandle);
	}
	else if (InputBuffer[0] != '#')
	{

		PUserPermission userPermissionNode = (PUserPermission)ExAllocatePoolWithTag(PagedPool, sizeof(UserPermission), '9gaT');

		if (userPermissionNode == NULL)
			return STATUS_UNSUCCESSFUL;

		RtlFillMemory(userPermissionNode->userProgramName, UserInputsLen, 0);
		RtlFillMemory(userPermissionNode->userExtension, UserInputsLen, 0);

		for (size_t i = 0; i < InputBufferLength; i++)
		{
			if (InputBuffer[i] == '#')
			{
				flag = 1;
				len = i + 1;//change here
				continue;
			}

			if (flag)
			{
				userPermissionNode->userExtension[i - len] = InputBuffer[i];
			}
			else
				userPermissionNode->userProgramName[i] = InputBuffer[i];
		}

		_wcsupr(userPermissionNode->userProgramName);
		userPermissionNode->next = NULL;

		status = AddUserPermissionList(userPermissionNode, &ListLockHandle);
	}
	else
	{
		InputBuffer++;
		status = RemoveUserPermissionList(InputBuffer, InputBufferLength - 1, &ListLockHandle);//Maybe InputBufferLength - 1 depends on gui behavior
	}

	CreateReturnedData();//Return the data even if status unsuccessful

	KeReleaseInStackQueuedSpinLock(&ListLockHandle);
	//<-touching the shared resource

	//KdPrint(("Kernel: userProgramName: %s ### userExtension: %s \r\n", userProgramName, userExtension));

	return STATUS_SUCCESS;
}

NTSTATUS IsPermissionUser(int isNewExtension)
{
	PUserPermission userPermissionNode = NULL;
	KLOCK_QUEUE_HANDLE ListLockHandle;

	//touching the shared resource->
	KeAcquireInStackQueuedSpinLock(&ListLock, &ListLockHandle);

	userPermissionNode = userPermissionHead;
	while (userPermissionNode != NULL)
	{
		if (wcsstr(gProgramName, userPermissionNode->userProgramName) != NULL && wcsstr(gExtension, userPermissionNode->userExtension) != NULL)//compare strings
		{
			KeReleaseInStackQueuedSpinLock(&ListLockHandle);
			return STATUS_SUCCESS;//permission granted	
		}

		if (wcsstr(gExtension, userPermissionNode->userExtension) != NULL)
			isNewExtension = 0;

		userPermissionNode = userPermissionNode->next;
	}

	if (isNewExtension)
	{
		KeReleaseInStackQueuedSpinLock(&ListLockHandle);
		return STATUS_SUCCESS;
	}

	KeReleaseInStackQueuedSpinLock(&ListLockHandle);
	//<-touching the shared resource

	return STATUS_UNSUCCESSFUL;
}

NTSTATUS IsProgramAllowedGlobal()
{
	WCHAR* ProgramNames[NumOfGlobalPermissions] = { L"TASKMGR.EXE", L"OPENWITH.EXE", L"SMARTSCREEN.EXE", L"RUNTIMEBROKER.EXE",
		L"DLLHOST.EXE", L"SVCHOST.EXE", L"SEARCHPROTOCOLHOST.EXE", L"SIHOST.EXE", L"CSRSS.EXE", L"SERVICES.EXE",
		L"EXPLORER.EXE", L"DBGVIEW.EXE", L"MANAGEMENTAGENTHOST.EXE", L"TIWORKER.EXE" };

	for (int i = 0; i < NumOfGlobalPermissions; i++)//system programs that are allowed to every extension
	{
		if (wcsstr(gProgramName, ProgramNames[i]) != NULL)//if the right program name
		{
			KdPrint(("Global Persmission granted: %ws ||| %ws\r\n", gExtension, ProgramNames[i]));
			return STATUS_SUCCESS;
		}
	}

	return STATUS_UNSUCCESSFUL;
}

NTSTATUS killProcess(_Inout_ PFLT_CALLBACK_DATA Data)
{
	NTSTATUS status;
	HANDLE ProcessHandle = NULL;
	OBJECT_ATTRIBUTES ObjectAttributes;
	CLIENT_ID ClientId;

	ObjectAttributes.Length = sizeof(OBJECT_ATTRIBUTES);
	ObjectAttributes.RootDirectory = NULL;
	ObjectAttributes.ObjectName = NULL;
	ObjectAttributes.Attributes = OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE;
	ObjectAttributes.SecurityDescriptor = NULL;
	ObjectAttributes.SecurityQualityOfService = NULL;

	ClientId.UniqueThread = 0;
	ClientId.UniqueProcess = (HANDLE)FltGetRequestorProcessId(Data);

	//KdPrint(("Process Id: %u\r\n", FltGetRequestorProcessId(Data)));//asks for the id of the process which requested the changes.

	status = ZwOpenProcess(
		&ProcessHandle,
		PROCESS_ALL_ACCESS,
		&ObjectAttributes,
		&ClientId);

	if (status == STATUS_SUCCESS)
	{
		status = ZwTerminateProcess(
			ProcessHandle,
			STATUS_SUCCESS);

		if (status)
			KdPrint(("[!] Could not to terminate process...\n"));
	}
	else
	{
		KdPrint(("[!] Could not to open process...\n"));
		return status;
	}
	return STATUS_SUCCESS;
}

NTSTATUS GetProcessImageName()
{
	NTSTATUS status;
	ULONG returnedLength;
	ULONG bufferLength;
	PVOID buffer;
	PUNICODE_STRING imageName;

	PAGED_CODE(); // this eliminates the possibility of the IDLE Thread/Process

	if (NULL == ZwQueryInformationProcess) {

		UNICODE_STRING routineName;

		RtlInitUnicodeString(&routineName, L"ZwQueryInformationProcess");

		ZwQueryInformationProcess =
			(QUERY_INFO_PROCESS)MmGetSystemRoutineAddress(&routineName);

		if (NULL == ZwQueryInformationProcess) {
			DbgPrint("Cannot resolve ZwQueryInformationProcess\n");
		}
	}

	//
	// Step one - get the size we need
	//

	status = ZwQueryInformationProcess(NtCurrentProcess(),
		ProcessImageFileName,
		NULL, // buffer
		0, // buffer size
		&returnedLength);

	if (STATUS_INFO_LENGTH_MISMATCH != status) {

		return status;

	}

	// If we get here, the buffer IS going to be big enough for us, so 
	// let's allocate some storage.

	buffer = ExAllocatePoolWithTag(PagedPool, returnedLength, 'ipgD');

	if (NULL == buffer) {

		return STATUS_INSUFFICIENT_RESOURCES;

	}

	//
	// Now lets go get the data
	//
	status = ZwQueryInformationProcess(NtCurrentProcess(),
		ProcessImageFileName,
		buffer,
		returnedLength,
		&returnedLength);

	if (NT_SUCCESS(status)) {
		//
		// Ah, we got what we needed
		//
		imageName = (PUNICODE_STRING)buffer;

		//KdPrint(("Program name is: %wZ\r\n", imageName));

		//RtlFillMemory(gProgramName, NameLen, 0);

		RtlCopyMemory(gProgramName, imageName->Buffer, imageName->Length);//copy buffer to array
		_wcsupr(gProgramName);//make uppercase

		//KdPrint(("Program name is: %ws\r\n", gProgramName));
	}

	//
	// free our buffer
	//
	ExFreePool(buffer);

	//
	// And tell the caller what happened.
	//    
	return status;
}

NTSTATUS PermissionCheck()
{
	size_t i, j, retPermission = 0;
	NTSTATUS status;

	Permission per1 = { .Extension = L"TXT",.ProgramNamesCounter = 2,.ProgramNames = { L"WORDPAD.EXE", L"NOTEPAD.EXE" } };
	Permission per2 = { .Extension = L"PNG",.ProgramNamesCounter = 3,.ProgramNames = { L"WMPLAYER.EXE", L"MICROSOFT.PHOTOS.EXE", L"MSPAINT.EXE" } };

	Permission* permissions[NumOfPermissions] = { &per1, &per2 };


	status = IsProgramAllowedGlobal();
	if (NT_SUCCESS(status))
		return STATUS_SUCCESS;


	for (i = 0; i < NumOfPermissions; i++)//every permission
	{
		if (wcsstr(gExtension, permissions[i]->Extension) != NULL)//if the right extension
		{
			retPermission = 1;
			break;
		}
	}

	if (retPermission)//An extension I mind
	{
		for (j = 0; j < (permissions[i]->ProgramNamesCounter); j++)//each program name in program names array of each permission.
		{
			if (wcsstr(gProgramName, permissions[i]->ProgramNames[j]) != NULL)//compare strings
			{
				KdPrint(("Persmission granted: %d %ws ||| %d %ws\r\n", i, permissions[i]->Extension, j, permissions[i]->ProgramNames[j]));
				return STATUS_SUCCESS;//permission granted
			}
		}

		status = IsPermissionUser(0);
	}
	else
	{
		status = IsPermissionUser(1);
	}

	if (NT_SUCCESS(status))
		return STATUS_SUCCESS;


	KdPrint(("Persmission not granted: %ws ||| %ws\r\n", gExtension, gProgramName));

	return STATUS_UNSUCCESSFUL;//permission denied
}

NTSTATUS Handler(_Inout_ PFLT_CALLBACK_DATA Data)
{
	PFLT_FILE_NAME_INFORMATION FileNameInfo;
	NTSTATUS status;

	status = FltGetFileNameInformation(Data, FLT_FILE_NAME_NORMALIZED | FLT_FILE_NAME_QUERY_DEFAULT, &FileNameInfo);

	if (NT_SUCCESS(status))
	{
		status = FltParseFileNameInformation(FileNameInfo);

		if (NT_SUCCESS(status))
		{
			if (FileNameInfo->Name.Length < NameLen)//change later
			{
				RtlFillMemory(gExtension, NameLen, 0);
				RtlFillMemory(gProgramName, NameLen, 0);

				RtlCopyMemory(gExtension, FileNameInfo->Extension.Buffer, FileNameInfo->Extension.Length);//check sizes !!!
				_wcsupr(gExtension);

				//KdPrint(("File suffix is: %ws\r\n", gExtension));

				status = GetProcessImageName();//finds program name
				if (NT_SUCCESS(status))
				{
					status = PermissionCheck();
					if (!NT_SUCCESS(status))//permmission denied
					{
						//KdPrint(("%ws was blocked!\r\n", gProgramName));

						//set parameters of operation to stop it
						Data->IoStatus.Status = STATUS_ACCESS_DENIED;
						Data->IoStatus.Information = 0;

						FltReleaseFileNameInformation(FileNameInfo);
						killProcess(Data);//kill the process

						return FLT_PREOP_COMPLETE;
					}
				}
			}
		}

		FltReleaseFileNameInformation(FileNameInfo);
	}

	return FLT_PREOP_SUCCESS_NO_CALLBACK;
}