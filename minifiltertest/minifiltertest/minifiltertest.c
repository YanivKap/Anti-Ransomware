/*++

Module Name:

	minifiltertest.c

Abstract:

	This is the main module of the minifiltertest miniFilter driver.

Environment:

	Kernel mode

--*/
#include <fltKernel.h>
#include <dontuse.h>
#include <suppress.h>

#include "Helper.h"

PFLT_FILTER gFilterHandle;
PFLT_PORT port = NULL;
PFLT_PORT Clientport = NULL;
PUserPermission userPermissionHead = NULL;
KSPIN_LOCK ListLock;

/*************************************************************************
	Prototypes
*************************************************************************/

DRIVER_INITIALIZE DriverEntry;
NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT DriverObject,
	_In_ PUNICODE_STRING RegistryPath
);

NTSTATUS
minifiltertestUnload(
	_In_ FLT_FILTER_UNLOAD_FLAGS Flags
);

FLT_PREOP_CALLBACK_STATUS
CreatePreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
);

FLT_PREOP_CALLBACK_STATUS
WritePreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
);

FLT_PREOP_CALLBACK_STATUS
SetInformationPreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
);

/*
FLT_POSTOP_CALLBACK_STATUS
CreatePostOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext,
	FLT_POST_OPERATION_FLAGS flags
);
*/

//
//  operation registration
//

CONST FLT_OPERATION_REGISTRATION Callbacks[] = {
	{ IRP_MJ_CREATE,
	0,
	CreatePreOperation,
	NULL },
	{ IRP_MJ_WRITE,
	  0,
	  WritePreOperation,
	  NULL },
	  { IRP_MJ_SET_INFORMATION,
	  0,
	  SetInformationPreOperation,
	  NULL },
	{ IRP_MJ_OPERATION_END }
};

//
//  This defines what we want to filter with FltMgr
//

CONST FLT_REGISTRATION FilterRegistration = {

	sizeof(FLT_REGISTRATION),         //  Size
	FLT_REGISTRATION_VERSION,           //  Version
	0,                                  //  Flags

	NULL,                               //  Context
	Callbacks,                          //  Operation callbacks

	minifiltertestUnload,                           //  MiniFilterUnload

	NULL,                    //  InstanceSetup
	NULL,            //  InstanceQueryTeardown
	NULL,            //  InstanceTeardownStart
	NULL,         //  InstanceTeardownComplete

	NULL,                               //  GenerateFileName
	NULL,                               //  GenerateDestinationFileName
	NULL                                //  NormalizeNameComponent

};

/*************************************************************************
IO port
*************************************************************************/

NTSTATUS MiniConnect(PFLT_PORT clientport, PVOID severportcookie, PVOID Context, ULONG size, PVOID Connectioncookie)
{
	Clientport = clientport;
	KdPrint(("Driver: Connected.\r\n"));

	return STATUS_SUCCESS;
}

VOID MiniDisconnect(PVOID connectioncookie)
{
	FltCloseClientPort(gFilterHandle, &Clientport);
	KdPrint(("Driver: Disconnected.\r\n"));
}

NTSTATUS MiniSendRec(PVOID portcookie, PVOID InputBuffer, ULONG InputBufferLength, PVOID OutputBuffer, ULONG OutputBufferLength, PULONG RetLength)
{
	NTSTATUS status;
	KdPrint(("User msg is: %s\r\n", (PCHAR)InputBuffer));

	status = ParseUserMsg(InputBuffer, InputBufferLength);

	if (NT_SUCCESS(status))
	{
		if (ReturnedLen < OutputBufferLength)
			strcpy((PCHAR)OutputBuffer, OutputBufferList);
	}

	return status;
}

/*************************************************************************
	MiniFilter initialization and unload routines.
*************************************************************************/

NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT DriverObject,
	_In_ PUNICODE_STRING RegistryPath
)
{
	NTSTATUS status;
	PSECURITY_DESCRIPTOR sd;
	OBJECT_ATTRIBUTES oa = { 0 };
	UNICODE_STRING name = RTL_CONSTANT_STRING(L"\\mf");

	userPermissionHead = NULL;

	UNREFERENCED_PARAMETER(RegistryPath);

	KdPrint(("minifiltertest! DriverEntry: Entered\n"));

	//
	//  Register with FltMgr to tell it our callback routines
	//

	status = FltRegisterFilter(DriverObject,
		&FilterRegistration,
		&gFilterHandle);

	FLT_ASSERT(NT_SUCCESS(status));

	if (!NT_SUCCESS(status))
	{
		return status;
	}

	KeInitializeSpinLock(&ListLock);//Spin Lock

	status = FltBuildDefaultSecurityDescriptor(&sd, FLT_PORT_ALL_ACCESS);

	if (NT_SUCCESS(status))
	{
		InitializeObjectAttributes(&oa, &name, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, sd);
		status = FltCreateCommunicationPort(gFilterHandle, &port, &oa, NULL, MiniConnect, MiniDisconnect, MiniSendRec, 1);

		FltFreeSecurityDescriptor(sd);

		if (NT_SUCCESS(status))
		{
			//
			//  Start filtering i/o
			//

			status = FltStartFiltering(gFilterHandle);

			if (NT_SUCCESS(status))
			{
				return status;
			}

			FltCloseCommunicationPort(port);
		}

		FltUnregisterFilter(gFilterHandle);
	}

	return status;
}

NTSTATUS
minifiltertestUnload(
	_In_ FLT_FILTER_UNLOAD_FLAGS Flags
)
{
	KdPrint(("minifiltertest! minifiltertestUnload: Entered \r\n"));
	FreeUserPermissionList();

	FltCloseCommunicationPort(port);
	FltUnregisterFilter(gFilterHandle);

	return STATUS_SUCCESS;
}

/*************************************************************************
	MiniFilter callback routines.
*************************************************************************/

//Create/Open pre-operation
FLT_PREOP_CALLBACK_STATUS
CreatePreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
	//KdPrint(("CreatePreOperation is running...\r\n"));

	return Handler(Data);
}

//Write pre-operation
FLT_PREOP_CALLBACK_STATUS
WritePreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
	//KdPrint(("WritePreOperation is running...\r\n"));

	return Handler(Data);
}

//Set information pre-operation
FLT_PREOP_CALLBACK_STATUS
SetInformationPreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
	//KdPrint(("SetInformationPreOperation is running...\r\n"));

	return Handler(Data);
}
